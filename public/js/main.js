$(document).ready(function() {

    var HOST = "http://192.168.0.107:7777/socket";
    var lhost = "http://192.168.0.107:8888";
    var moduleCount = 0;

    var sock = new SockJS(HOST);

    function log(data) {
        var consoleId = "#console";

        $(consoleId).append("<h6>" + JSON.stringify(data) + "</h6>");
        $(consoleId).animate({ scrollTop: $(consoleId)[0].scrollHeight}, 100);


    }

    sock.onopen = function() {
        $("#connectionStatus").first().html("Connected").addClass('success');
    };
    sock.onmessage = function(e) {
        log(e.data);
        var obj = JSON.parse(e.data);
        if (obj.cmd == 'installModule') {
            log('installing some module with id:' + obj.moduleId);

            var moduleData = '<br clear="all"/>';
            moduleData += '<div id="module' + obj.moduleId + '" class="nine column">';
            moduleData += '<h4>module' + obj.moduleId + '</h4>';
            moduleData += '<iframe width="1300" height="300" src="' + lhost + "/module/" + obj.moduleId + '/index.html"></iframe>';
            moduleData += '</div>';

            $('#modules').append(moduleData);


        } else if (obj.cmd == 'removeModule') {
            log('removing some module with id:' + obj.moduleId);

            console.log($('#module' + obj.moduleId));
            $('#module' + obj.moduleId).remove();

        }
    };
    sock.onclose = function() {

        $("#connectionStatus").first().html("Disconnected").addClass('fail');

    };


});
