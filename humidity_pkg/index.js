var http = require('http');
var sockjs = require('sockjs');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

var TestModule = function() {
    this.data = 1;
    var that = this;
    this.on('recSerialData', function() {

console.log("goga is heree");
    });
    this.on('recSocketData', function(data) {

    });

    this.interval = setInterval(function() {
        that.emit('sendSerialData', "Humidity Module is sending data to serial");
	var data = {};	
	data.moduleMessage = "From Humidity module sending to socket! ";
	that.emit('sendSocketData', data);
    }, 5000);

    this.deconstruct = function(){
        clearInterval(that.interval);          
    };
};

util.inherits(TestModule, EventEmitter);

module.exports = TestModule;
