var http = require('http');
var sockjs = require('sockjs');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

var SocketManager = function() {
    var that = this;
    this.server = sockjs.createServer({
        sockjs_url: 'http://cdn.jsdelivr.net/sockjs/0.3.4/sockjs.min.js'
    });
    this.connections = [];

    this.netServer = http.createServer();

    this.server.installHandlers(this.netServer, {
        prefix: '/socket'
    });

    this.server.on('connection', function(conn) {
        var connId = that.connections.length;
        that.connections[connId] = conn;

        that.emit('connection', connId);

        console.log('connection!');

        conn.on('data', function(data) {
            that.emit('data', data, connId);
            console.log(data);
        });

        conn.on('close', function() {
            delete that.connections[connId];
            that.emit('close', connId);
        });
    });

    this.start = function() {
        that.netServer.listen(7777, 'localhost');
    };

    this.send = function(data, connId) {
        data = JSON.stringify(data);
        if (connId) {
            try {
                that.connections[connId].write(data);
                return;
            } catch (ex) {
                delete that.connections[connId];
                that.emit('close', connId);
            }
        }
        that.connections.forEach(function(conn, index) {
            try {
                conn.write(data);
            } catch (ex) {
                delete that.connections[index];
                that.emit('close', index);
            }
        });
    };
};

util.inherits(SocketManager, EventEmitter);

module.exports = SocketManager;
