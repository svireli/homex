var util = require('util');
var EventEmitter = require('events').EventEmitter;

var FakeSerial = function(eventEmmiter) {

    var self = this;

    if (eventEmmiter) {
        self.ee = eventEmmiter;
    }

    self.sensArr = ['sens1', 'sens2', 'sens3', 'sens4', 'sens5', 'sens6', 'sens7', 'sens8'];

    self.random = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    self.emitRandomData = function() {
        self.emit('data', self.sensArr[self.random(0, (self.sensArr.length - 1))] + "|" + self.random(1567, 23452345));
    };

    self.emitNewSensor = function() {
        self.emit('newSensor', self.sensArr[self.random(0, (self.sensArr.length - 1))]);
    };
    this.send = function(data){
        console.log('sending to serial: ', data);
    };
};

util.inherits(FakeSerial, EventEmitter);

module.exports = FakeSerial;
